package com.sample.gson;

import java.lang.reflect.Type;
import java.util.List;

import com.google.gson.Gson;
import com.google.gson.annotations.SerializedName;
import com.google.gson.reflect.TypeToken;

public class Tester {
	
	static String user1 = "{'name':'�ֵ�kidou','age':24,'emailAddress':'ikidou@example.com'}";
	static String user2 = "{'name':'�ֵ�kidou','age':24,'email_address':'ikidou@example.com'}";
	static String user3 = "{'name':'�ֵ�kidou','age':24,'email':'ikidou@example.com'}";

	public static void main(String[] args) {
		/*Gson gson = new Gson();
		int i = gson.fromJson("100", int.class);              //100
		double d = gson.fromJson("\"99.99\"", double.class);  //99.99
		boolean b = gson.fromJson("true", boolean.class);     // true
		String str = gson.fromJson("String", String.class);   // String*/		
		
		/*Gson gson = new Gson();
		User user = new User("�ֵ�kidou",24);
		String jsonObject = gson.toJson(user); // {"name":"�ֵ�kidou","age":24}
		System.out.println(jsonObject);
		User fromJson = gson.fromJson(jsonObject, User.class);
		System.out.println(fromJson.toString());
		
		User userbean1 = gson.fromJson(user3, User.class);
		System.out.println(userbean1.toString());*/
		
		// Gson��ʹ�÷���
		/*Gson gson = new Gson();
		String jsonArray = "[\"Android\",\"Java\",\"PHP\"]";
		String[] strings = gson.fromJson(jsonArray, String[].class);
		System.out.println(strings[0]);*/
		
		/*Gson gson = new Gson();
		String jsonArray = "[\"Android\",\"Java\",\"PHP\"]";
		String[] strings = gson.fromJson(jsonArray, String[].class);
		List<String> stringList = gson.fromJson(jsonArray, new TypeToken<List<String>>() {}.getType());
		System.out.println(stringList.get(0));*/
		
		String json1 = "{'code':'0','message':'success','data':{'name':'�ֵ�kidou','age':24,'emailAddress':'ikidou@example.com'}}";
		Gson gson = new Gson();
		UserResult userResult = gson.fromJson(json1,UserResult.class);
		User user = userResult.data;
		System.out.println(user.toString());

		String json2 = "{'code':'0','message':'success','data':[{'name':'�ֵ�kidou','age':24,'email_address':'ikidou@example.com'}, {'name':'�ֵ�kidou','age':24,'email_address':'ikidou@example.com'}]}";
		UserListResult userListResult = gson.fromJson(json2,UserListResult.class);
		List<User> users = userListResult.data;
		System.out.println(users.toString());
		
		//�����ظ�����Result��
		Type userType = new TypeToken<Result<User>>(){}.getType();
		Result<User> userRes = gson.fromJson(json1,userType);
		User userInfo = userRes.data;
		System.out.println(userInfo.toString());
		
		Type userListType = new TypeToken<Result<List<User>>>(){}.getType();
		Result<List<User>> userListRes2 = gson.fromJson(json2,userListType);
		List<User> userList = userListRes2.data;
		System.out.println(userList.toString());
		System.out.println(gson.toJson(userListRes2));
	}

}

class Result<T> {
    public int code;
    public String message;
    public T data;
}

class UserResult {
    public int code;
    public String message;
    public User data;
}
//=========
class UserListResult {
    public int code;
    public String message;
    public List<User> data;
}

class User {
    //ʡ������
    public String name;
    public int age;
    @SerializedName(value = "emailAddress", alternate = {"email", "email_address"})
    public String emailAddress;
    
    public User(){}
    
    public User(String name, int age){
    	this.name = name;
    	this.age = age;
    }
    
    public User(String name, int age, String email){
    	this.name = name;
    	this.age = age;
    	this.emailAddress = email;
    }

	@Override
	public String toString() {
		return "User [name=" + name + ", age=" + age + ", emailAddress=" + emailAddress + "]";
	}
    
}
