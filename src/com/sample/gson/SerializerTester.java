package com.sample.gson;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStream;
import java.io.OutputStreamWriter;
import java.io.StringReader;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.stream.JsonReader;
import com.google.gson.stream.JsonWriter;

/**
 * Gson的流式序列化
 *
 */
public class SerializerTester {

	public static void main(String[] args) throws IOException {
		// 手动方式
		String json = "{\"name\":\"怪盗kidou\",\"age\":\"24\"}";
		User user = new User();
		JsonReader reader = new JsonReader(new StringReader(json));
		reader.beginObject(); // throws IOException
		while (reader.hasNext()) {
		    String s = reader.nextName();
		    switch (s) {
		        case "name":
		            user.name = reader.nextString();
		            break;
		        case "age":
		            user.age = reader.nextInt(); //自动转换
		            break;
		        case "email":
		            user.emailAddress = reader.nextString();
		            break;
		    }
		}
		reader.endObject(); // throws IOException
		reader.close();
		System.out.println(user.name);  // 怪盗kidou
		System.out.println(user.age);   // 24
		System.out.println(user.emailAddress); // ikidou@example.com
		
		Gson gson = new Gson();
		User user2 = new User("怪盗kidou",24,"ikidou@example.com");
		gson.toJson(user2,System.out); // 写到控制台
		
		File file = new File("user.log");
		OutputStream fos = new FileOutputStream(file);
		//自动方式
		JsonWriter writer = new JsonWriter(new OutputStreamWriter(System.out));
		writer.beginObject() // throws IOException
		        .name("name").value("怪盗kidou")
		        .name("age").value(24)
		        .name("email").nullValue() //演示null
		        .endObject(); // throws IOException
		writer.flush(); // throws IOException
		//{"name":"怪盗kidou","age":24,"email":null}
		
		System.out.println();
		System.out.println("======================================");
		
		// 使用GsonBuilder导出null值、格式化输出、日期时间
		Gson gs = new GsonBuilder()
		        .serializeNulls()
		        .create();
		User u = new User("怪盗kidou", 24);
		System.out.println(gs.toJson(u)); //{"name":"怪盗kidou","age":24,"email":null}
		
		Gson g = new GsonBuilder()
		        //序列化null
		        .serializeNulls()
		        // 设置日期时间格式，另有2个重载方法
		        // 在序列化和反序化时均生效
		        .setDateFormat("yyyy-MM-dd")
		        // 禁此序列化内部类
		        .disableInnerClassSerialization()
		        //生成不可执行的Json（多了 )]}' 这4个字符）
		        .generateNonExecutableJson()
		        //禁止转义html标签
		        .disableHtmlEscaping()
		        //格式化输出
		        .setPrettyPrinting()
		        .create();
	}

}
