package com.sample.gson;

import java.io.IOException;
import java.lang.reflect.Type;
import java.util.ArrayList;
import java.util.List;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.JsonElement;
import com.google.gson.JsonPrimitive;
import com.google.gson.JsonSerializationContext;
import com.google.gson.JsonSerializer;
import com.google.gson.TypeAdapter;
import com.google.gson.reflect.TypeToken;
import com.google.gson.stream.JsonReader;
import com.google.gson.stream.JsonWriter;

public class AdapterTester {

	public static void main(String[] args) {
		/*User user = new User("怪盗kidou", 24);
		user.emailAddress = "ikidou@example.com";
		Gson gson = new GsonBuilder()
		        //为User注册TypeAdapter
		        .registerTypeAdapter(User.class, new UserTypeAdapter())
		        .create();
		System.out.println(gson.toJson(user));*/
		
		JsonSerializer<Number> numberJsonSerializer = new JsonSerializer<Number>() {
		    @Override
		    public JsonElement serialize(Number src, Type typeOfSrc, JsonSerializationContext context) {
		        return new JsonPrimitive(String.valueOf(src));
		    }
		};
		Gson gson = new GsonBuilder()
		        .registerTypeAdapter(Integer.class, numberJsonSerializer)
		        .registerTypeAdapter(Long.class, numberJsonSerializer)
		        .registerTypeAdapter(Float.class, numberJsonSerializer)
		        .registerTypeAdapter(Double.class, numberJsonSerializer)
		        .create();
		System.out.println(gson.toJson(100.0));//结果："100.0"
		
		
		// 注：如果一个被序列化的对象本身就带有泛型，且注册了相应的TypeAdapter，那么必须调用Gson.toJson(Object,Type)，明确告诉Gson对象的类型。
		/*Type type = new TypeToken<List<User>>() {}.getType();
		TypeAdapter typeAdapter = new TypeAdapter<List<User>>() {
			//略
			@Override
			public List<User> read(JsonReader arg0) throws IOException {
				// TODO Auto-generated method stub
				return null;
			}

			@Override
			public void write(JsonWriter writeer, List<User> arg1) throws IOException {
				// TODO Auto-generated method stub
				
			}
		   
		};
		Gson g = new GsonBuilder()
		        .registerTypeAdapter(type, typeAdapter)
		        .create();
		List<User> list = new ArrayList<>();
		list.add(new User("a",11));
		list.add(new User("b",22));
		//注意，多了个type参数
		String result = g.toJson(list, type);*/
		
		
	}

}

class UserTypeAdapter extends TypeAdapter<User> {

    @Override
    public void write(JsonWriter out, User value) throws IOException {
        out.beginObject();
        out.name("name").value(value.name);
        out.name("age").value(value.age);
        out.name("email").value(value.emailAddress);
        out.endObject();
    }

    @Override
    public User read(JsonReader in) throws IOException {
        User user = new User();
        in.beginObject();
        while (in.hasNext()) {
            switch (in.nextName()) {
                case "name":
                    user.name = in.nextString();
                    break;
                case "age":
                    user.age = in.nextInt();
                    break;
                case "email":
                case "email_address":
                case "emailAddress":
                    user.emailAddress = in.nextString();
                    break;
            }
        }
        in.endObject();
        return user;
    }
}
